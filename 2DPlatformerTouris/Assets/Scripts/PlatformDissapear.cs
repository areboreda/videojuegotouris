﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformDissapear : MonoBehaviour {

	private Animator animator;

	void Awake () {
		animator = GetComponent<Animator> ();
	}

	void OnTriggerEnter2D (Collider2D col) {
		if (col.tag == "Player") 
		{
			animator.SetBool ("Destroy", true);
			Destroy (this.gameObject, 1);
		}
	}
}
