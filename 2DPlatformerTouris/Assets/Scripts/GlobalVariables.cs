﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalVariables {

	public static Transform respawn;
	public static GameObject InitialSpawnPoint;
	public static bool fade;
	public static bool alive = true;
	public static bool shake;
	public static float tiempoSegundos = 60;
	public static float shakeDuration = 0.5f;
	public static float shakeAmount = 0.7f;
	public static float decreaseFactor = 1.0f;
	public static string zoneName = "1-1";
	public static GameObject zone;

}



