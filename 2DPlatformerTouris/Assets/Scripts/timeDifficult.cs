﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class timeDifficult : MonoBehaviour {

	public float tiempoMundo = 20f;

	public GameObject player;
	public Text segundos;

	public Animator anim;

	void Start ()
	{
		GlobalVariables.tiempoSegundos = tiempoMundo;
		player = GameObject.Find ("Player");
		InvokeRepeating ("restar", 0f, 1f);
		anim = GetComponentInChildren<Animator> ();
	}

	void Update ()
	{
		if (GlobalVariables.tiempoSegundos < 0) 
		{
			GlobalVariables.alive = false;
			Animator playerAnimator = player.GetComponent<Animator>();
			playerAnimator.SetBool ("death", true);
			player.GetComponent<PlayerPlatformerController>().enabled = false;
			StartCoroutine (DeathByTime ());
		}
		/*if (!GlobalVariables.alive)
		{
			GlobalVariables.tiempoSegundos = tiempoMundo;
			anim.SetBool ("Increase",false);
			anim.SetBool ("Increase2",false);
			anim.SetBool ("Increase3",false);
			anim.SetTrigger ("Reset");
			GlobalVariables.alive = true;
		}*/


		//efectos visuales

			if (GlobalVariables.tiempoSegundos == 40)
			{
				GlobalVariables.shakeAmount = 0.05f;
				GlobalVariables.shakeDuration = 0.5f;
				GlobalVariables.shake = true;
				anim.SetBool ("Increase",true);
			}
			if (GlobalVariables.tiempoSegundos == 20)
			{
				GlobalVariables.shakeAmount = 0.1f;
				GlobalVariables.shakeDuration = 1;
				GlobalVariables.shake = true;
				anim.SetBool ("Increase2",true);
				anim.SetBool ("Increase",false);
			}
			if (GlobalVariables.tiempoSegundos == 5)
			{
				GlobalVariables.shakeAmount = 0.2f;
				GlobalVariables.shakeDuration = 2;
				GlobalVariables.shake = true;
				anim.SetBool ("Increase3",true);
				anim.SetBool ("Increase2",false);
			}
			
	}

	void restar()
	{
		if (GlobalVariables.alive)
		{
			segundos.text = "" + GlobalVariables.tiempoSegundos;
			GlobalVariables.tiempoSegundos -= 1;
		}

	}

	IEnumerator DeathByTime()
	{
		yield return new WaitForSeconds (3);
		GlobalVariables.alive = true;
		player.transform.position = GlobalVariables.InitialSpawnPoint.transform.position;
		player.GetComponent<PlayerPlatformerController>().enabled = true;
		Animator playerAnimator = player.GetComponent<Animator>();
		playerAnimator.SetBool ("death", false);
		anim.SetBool ("Increase",false);
		anim.SetBool ("Increase2",false);
		anim.SetBool ("Increase3",false);
		anim.SetTrigger ("Reset");
		GlobalVariables.tiempoSegundos = tiempoMundo;
	}
	}

