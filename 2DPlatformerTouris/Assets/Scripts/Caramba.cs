﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Caramba : MonoBehaviour {

	public Transform carambanoTransform;

	public float visionRadius;
	public float attackRadius;
	public float speed;
	private bool start = true;
	private bool falling = false;
	private bool trembling = true;
		
	Vector3 initialPosition, target;
	Vector3 originalPos;

	GameObject player;
	Rigidbody2D rb2d;

	void Start () {
		player = GameObject.FindGameObjectWithTag("Player");
		originalPos = carambanoTransform.transform.position;
		initialPosition = transform.position;
		rb2d = GetComponent<Rigidbody2D>();
	}

	void Update () {

		target = initialPosition;

		RaycastHit2D hit = Physics2D.Raycast(
			transform.position, 
			player.transform.position - transform.position, 
			visionRadius, 
			1 << LayerMask.NameToLayer("Default") 
		);

		// Raycast radius
		Vector3 forward = transform.TransformDirection(player.transform.position - transform.position);
		Debug.DrawRay(transform.position, forward, Color.red);

		if (hit.collider != null) {
			if (hit.collider.tag == "Player"){
				target = player.transform.position;
			}
		}

		float distance = Vector3.Distance(target, transform.position);
		Vector3 dir = (target - transform.position).normalized;

		if (target != initialPosition && distance < attackRadius){
			Fall ();
		}

		Debug.DrawLine(transform.position, target, Color.green);
	}
		
	void Fall() 
	{
		if (start) 
		{
			StartCoroutine (Falling ());
			start = false;
		}
		if (trembling)
		{
			rb2d.constraints = RigidbodyConstraints2D.None;
			rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;

			carambanoTransform.transform.position = originalPos + Random.insideUnitSphere * 0.02f;
		}
		if (falling)
		{
			rb2d.gravityScale = 1;
		}
	}

	IEnumerator Falling()
	{
		yield return new WaitForSeconds (1.5f);
		trembling = false;
		falling = true;
	}
	// Radius vision and attack
	void OnDrawGizmosSelected() {

		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(transform.position, visionRadius);
		Gizmos.DrawWireSphere(transform.position, attackRadius);

	}


}