﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPlatformerController : PhysicsObject {


	public float maxSpeed = 7f;
	public float jumpTakeOffSpeed = 7f;

	protected bool grabbedRight;
	protected bool grabbedLeft;
	public bool crouched;
	public bool running;
	public bool sliding;
	public bool facingRight; 
	public bool checkSlide = false;
	private bool spriteRight = true; 
	private bool used;
	public bool stucked;
	public bool OnIce;

	private GameObject player;
	private SpriteRenderer sprite;
	private Animator animator;
	// Use this for initialization
	void Awake () {
		GlobalVariables.InitialSpawnPoint = GameObject.Find ("InitialSpawnPoint");
		animator = GetComponent<Animator> ();
		sprite = GetComponent<SpriteRenderer> ();
		player = GameObject.FindGameObjectWithTag("Player");
	}
		

	protected override void ComputeVelocity()
	{
		Vector2 move = Vector2.zero;
		// Movimiento personaje
		if (!GlobalVariables.fade)
		{
			if (!crouched) 
			{
				if (OnIce) 
				{
					move.x = Input.GetAxis ("Horizontal") * 3;
				}

				if (sliding) 
				{
					// Comprobar en que direccion se esta moviendo
					if (!checkSlide) 
					{
						move.x = Input.GetAxis ("Horizontal") ;
						if (move.x < 0.01) {
							facingRight = false;
						} else if (move.x > 0.01)
						{
							facingRight = true;
						}
						checkSlide = true;
					}
					// Si se mueve hacia la izquierda
					if (!facingRight)
					{
						move.x = Input.GetAxis ("Horizontal");
						if (move.x < 0.01) {
							move.x = Input.GetAxis ("Horizontal") * 1.5f;
						} else if (move.x > 0.01)
						{
							move.x = 0;
						}
					}
					// Si se mueve hacia la derecha
					else if (facingRight)
					{
						move.x = Input.GetAxis ("Horizontal");
						if (move.x < 0.01) {
							move.x = 0;
						} else if (move.x > 0.01)
						{
							move.x = Input.GetAxis ("Horizontal") * 1.5f;
						}
					}

				} else
				{
					if (Input.GetButton ("Run") && grounded) {
						move.x = Input.GetAxis ("Horizontal") * 1.5f;
						running = true;
						animator.SetBool ("running", running);
					} else if (!OnIce)
					{
						move.x = Input.GetAxis ("Horizontal");
						running = false;
						animator.SetBool ("running", running);
					}
				}


			}
		}


		// Finaliza movimiento

		//FLip sprite

		if (move.x > 0 && !spriteRight) {
			Flip ();
		} else if (move.x < 0 && spriteRight) 
		{
			Flip();
		}
			

		if (Input.GetButtonDown ("Crouch") && grounded && !running) 
		{
			used = true;
			crouched = true;
			animator.SetBool ("crouch", crouched);
			this.gameObject.tag = "Invisible";
			col2D.size = new Vector2(col2D.size.x, col2D.size.y - 0.25f);
			col2D.offset = new Vector2(col2D.offset.x, col2D.offset.y - 0.12f);
		}
		if (Input.GetButtonUp ("Crouch") && used) {
			used = false;
			crouched = false;
			animator.SetBool ("crouch", crouched);
			this.gameObject.tag = "Player";
			col2D.size = new Vector2(col2D.size.x, col2D.size.y + 0.25f);
			col2D.offset = new Vector2(col2D.offset.x, 0);
		}

		// Salto y me deslizo
		if (Input.GetButtonDown ("Jump") && running && grounded && !sliding) {
			animator.SetBool ("slide", true);
			col2D.size = new Vector2(col2D.size.x + 0.65f, col2D.size.y - 0.3f);
			col2D.offset = new Vector2(col2D.offset.x, col2D.offset.y - 0.15f);
			sliding = true;
			StartCoroutine (Slide());
		}

		//Salto en barro o nieve
		if (Input.GetButtonDown ("Jump") && grounded && !crouched && !running && stucked) 
		{
			velocity.y = jumpTakeOffSpeed/2;
		}

		//Salto normal y gradual
		if (Input.GetButtonDown ("Jump") && grounded && !crouched && !running && !stucked) {
			velocity.y = jumpTakeOffSpeed;
		}
		else if (Input.GetButtonUp ("Jump"))
		{
			if (velocity.y > 0) 
			{
				velocity.y = velocity.y * .5f;
			}
		}

		if (Input.GetButtonDown ("Jump") && grabbedRight)
		{
			rb2d.isKinematic = false;
			rb2d.AddForce (new Vector2 (-10f, 10f), ForceMode2D.Impulse);
			StartCoroutine (IsKinematic ());
		}

		if (Input.GetButtonDown ("Jump") && grabbedLeft)
		{
			rb2d.isKinematic = false;
			rb2d.AddForce (new Vector2 (10f, 10f), ForceMode2D.Impulse);
			StartCoroutine (IsKinematic ());
		}

		animator.SetBool ("grounded", grounded);
		animator.SetFloat ("velocityX", Mathf.Abs (velocity.x) / maxSpeed);

		targetVelocity = move * maxSpeed;

	}

	//Flipsprite
	void Flip ()
	{
		spriteRight = !spriteRight;

		if (!spriteRight) {
			sprite.flipX = true;
		} else if (spriteRight) 
		{
			sprite.flipX = false;
		}
	}
		
	void OnTriggerEnter2D (Collider2D col)
	{
		if (col.tag == "Ice")
		{
			OnIce = true;
		}

		if (col.tag == "Stuck")
		{
			stucked = true;
		}

		if (col.tag == "Respawn")
		{
			GlobalVariables.respawn = col.transform;
			Debug.Log (GlobalVariables.respawn);
		}

		if (col.tag == "ClimbRight") 
		{
			velocity.y = 0;
			gravityModifier = 0.1f;
			grabbedRight = true;
			animator.SetBool ("grabbed", true);

		}

		if (col.tag == "ClimbLeft") 
		{
			velocity.y = 0;
			gravityModifier = 0.1f;
			grabbedLeft = true;
			animator.SetBool ("grabbed", true);

		}



		 if (col.tag == "Kill") 
		{
			player.transform.position = GlobalVariables.respawn.position;
			/* Por si quiero que haga reset al morir normal
			GlobalVariables.alive = false;*/
		}  



	}

	void OnTriggerExit2D (Collider2D col)
	{
		if (col.tag == "ClimbRight")
		{
			gravityModifier = 1f;
			grabbedRight = false;
			animator.SetBool ("grabbed", false);
		}
		if (col.tag == "ClimbLeft")
		{
			gravityModifier = 1f;
			grabbedLeft = false;
			animator.SetBool ("grabbed", false);

		}
		if (col.tag == "Stuck")
		{
			stucked = false;
		}

		if (col.tag == "Ice")
		{
			OnIce = false;
		}
	}
		
	IEnumerator Slide()
	{
		yield return new WaitForSeconds (0.5f);
		animator.SetBool ("slide", false);
		col2D.size = new Vector2(col2D.size.x - 0.65f, col2D.size.y + 0.3f);
		col2D.offset = new Vector2(col2D.offset.x, 0);
		sliding = false;
		checkSlide = false;
	}

	IEnumerator IsKinematic()
	{
		yield return new WaitForSeconds (0.2f);
		rb2d.velocity = new Vector2 (0, 0);
		rb2d.isKinematic = true;
	}
		



}
