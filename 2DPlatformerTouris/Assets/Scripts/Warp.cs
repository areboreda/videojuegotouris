﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Warp : MonoBehaviour
{
	// Para almacenar el punto de destino
	public GameObject target;
	public GameObject targetMap;
	public GameObject camTransform;
	// Para controlar si empieza o no la transición
	bool start = false;
	// Para controlar si la transición es de entrada o salida
	bool isFadeIn = false;
	// Opacidad inicial del cuadrado de transición
	float alpha = 0;
	// Transición de 1 segundo
	float fadeTime = 1f;

	void Awake ()
	{
		Assert.IsNotNull(target);

		GetComponent<SpriteRenderer> ().enabled = false;
		transform.GetChild (0).GetComponent<SpriteRenderer> ().enabled = false;

	}


	IEnumerator OnTriggerEnter2D (Collider2D col) {
		// Al chocar el jugador contra el warp comprobamos si es el jugador y lo transportamos
		if (col.CompareTag ("Player")) {

			// Empezamos la trancisión fadeIn y desactivamos los controles de animación y movimiento
			FadeIn ();
			GlobalVariables.fade= true;
			GlobalVariables.zone = GameObject.Find (targetMap.name);
			Debug.Log (GlobalVariables.zone);
			// Esperamos el tiempo que dura la transición
			yield return new WaitForSeconds (fadeTime);
			camTransform.transform.position = new Vector3(GlobalVariables.zone.transform.position.x,GlobalVariables.zone.transform.position.y,GlobalVariables.zone.transform.position.z - 10);
			// Actualizamos la posición y cámara, deshacemos transición y reactivamos los controles
			col.transform.position = target.transform.GetChild (0).transform.position;
			FadeOut ();
			GlobalVariables.fade = false;
			GlobalVariables.zoneName = targetMap.name;
		
		}
	}

	// Dibujaremos un cuadrado con opacidad encima de la pantalla simulando una transición
	void OnGUI () {

		// Si no empieza la transición salimos del evento directamente
		if (!start)
			return;

		// Si ha empezamos creamos un color con una opacidad inicial a 0
		GUI.color = new Color (GUI.color.r, GUI.color.g, GUI.color.b, alpha);

		// Creamos una textura temporal para rellenar la pantalla
		Texture2D tex;
		tex = new Texture2D (1, 1);
		tex.SetPixel (0, 0, Color.black);
		tex.Apply ();

		// Dibujamos la textura sobre toda la pantalla
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), tex);

		// Controlamos la transparencia
		if (isFadeIn) {
			// Si es la de aparecer le sumamos opacidad
			alpha = Mathf.Lerp (alpha, 1.1f, fadeTime * Time.deltaTime);
		} else {
			// Si es la de desaparecer le restamos opacidad
			alpha = Mathf.Lerp (alpha, -0.1f, fadeTime * Time.deltaTime);
			// Si la opacidad llega a 0 desactivamos la transición
			if (alpha < 0) start = false;
		}

	}

	// Método para activar la transición de entrada
	void FadeIn () {
		start = true;
		isFadeIn = true;
	}

	// Método para activar la transición de salida
	void FadeOut () {
		isFadeIn = false;
	}
		
}