﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{
	public Transform camTransform;



	Vector3 originalPos;

	void Awake()
	{
		if (camTransform == null)
		{
			camTransform = GetComponent<Transform> ();
		}

		originalPos = camTransform.transform.position;
	}

	void Position()
	{
		
	}

	void Update()
	{
		if (GlobalVariables.shake) {
			if (GlobalVariables.shakeDuration > 0) {
				camTransform.localPosition = originalPos + Random.insideUnitSphere * GlobalVariables.shakeAmount;

				GlobalVariables.shakeDuration -= Time.deltaTime * GlobalVariables.decreaseFactor;
			} else {
				GlobalVariables.shakeDuration = 0f;
				camTransform.localPosition = originalPos;
				GlobalVariables.shake = false;
			}
		} else 
		{
			originalPos = camTransform.transform.position;
		}

	}
}