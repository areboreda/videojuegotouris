﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMoveFar : MonoBehaviour {

	private float move;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update() {
		move = Input.GetAxis ("Horizontal") ;
		// Move the object to the right relative to the camera 1 unit/second.
		if (move > 0)
		{
			transform.Translate(Vector3.right * Time.deltaTime * 0.5f, Camera.main.transform);
		}else if (move < 0)
		{
			transform.Translate(Vector3.left * Time.deltaTime * 0.5f, Camera.main.transform);
		}

	}
}
