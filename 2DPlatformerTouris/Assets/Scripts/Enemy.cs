﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	public float visionRadius;
	public float attackRadius;
	public float speed;
	public bool spriteRight; 
	public float attackSpeed = 0.2f;
	bool attacking;

	Vector3 initialPosition, target;

	GameObject player;
	Animator anim;
	Rigidbody2D rb2d;

	void Start () {

		player = GameObject.FindGameObjectWithTag("Player");

		initialPosition = transform.position;

		anim = GetComponent<Animator>();
		rb2d = GetComponent<Rigidbody2D>();
	}

	void Update () {

		target = initialPosition;

		RaycastHit2D hit = Physics2D.Raycast(
			transform.position, 
			player.transform.position - transform.position, 
			visionRadius, 
			1 << LayerMask.NameToLayer("Default") 
		);

		// Raycast radius
		Vector3 forward = transform.TransformDirection(player.transform.position - transform.position);
		Debug.DrawRay(transform.position, forward, Color.red);

		if (hit.collider != null) {
			if (hit.collider.tag == "Player"){
				target = player.transform.position;
			}
		}
			
		float distance = Vector3.Distance(target, transform.position);
		Vector3 dir = (target - transform.position).normalized;

		if (target != initialPosition && distance < attackRadius){
			anim.SetBool ("attack",true);
			if (!attacking) StartCoroutine(Attack(attackSpeed));

		}
		else if (!attacking){
			rb2d.MovePosition(transform.position + dir * speed * Time.deltaTime);

			anim.speed = 1;
			anim.SetBool("walking", true);

			if (dir.x > 0 && !spriteRight) {
				Flip ();
			} else if (dir.x < 0 && spriteRight) 
			{
				Flip();
			}
		}
			
		if (target == initialPosition && distance < 0.02f){
			transform.position = initialPosition; 
			anim.SetBool("walking", false);
		}

		// Line green
		Debug.DrawLine(transform.position, target, Color.green);
	}

	void Flip ()
	{
		spriteRight = !spriteRight;

		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	// Radius vision and attack
	void OnDrawGizmosSelected() {

		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(transform.position, visionRadius);
		Gizmos.DrawWireSphere(transform.position, attackRadius);

	}

	IEnumerator Attack(float seconds){
		attacking = true;  // Activamos la bandera
		if (target != initialPosition) {
			Vector3 playerPosition = GlobalVariables.respawn.position;
			player.transform.position = playerPosition;
			GlobalVariables.alive = false;
			yield return new WaitForSeconds(seconds);
		}
		anim.SetBool ("attack",false);
		attacking = false; // Desactivamos la bandera
	}


}