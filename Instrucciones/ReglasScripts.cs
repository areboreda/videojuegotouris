﻿//Declaracion de librerias

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReglasScripts : MonoBehaviour {

	//Declaracion de variables:

	// Reglas de nomenclatura generales:

	/* Las variables siempre asi enemyDead o enemyDead_1, en cuanto a los scripts ReglasScripts y en cuanto a las funciones FindObject, acordaos siempre de dejar comentarios en cosas que creais que no se entiendan bien
	 o que pueden dar lugar a confusion.*/

	//Declaracion de gameObjects
	public GameObject enemyDead;

	//Declaracion de componentes de gameObjects
	private Collider2D boxCollider;


	//Declaracion de variables tipo float, int, bool...
	public float maxSpeed = 7;
	public bool running = false;

	//Declaracion de audio
	public AudioSource audioJump;

	// Primero escribimos los void de inicialización 
	void Start () {}

	void Awake () {}

	// Aqui colocariamos void que no sean de las caracteristicas siguientes pero que por logica de interpretacion se ejecutarian al comienzo del programa
	void FindObject (){}

	// Luego los void que se ejecutaran en cada frame
	void Update () {}

	void FixedUpdate () {}

	// Aqui los void que detecten colisiones o triggers
	void OnTriggerEnter2D(){}

	void OnTriggerExit2D(){}

	// Seguiriamos con las coroutines
	IEnumerator Monedas(){}

	// Por ultimo los void que no esten englobados en los otros aspectos
	void M(){}
}
